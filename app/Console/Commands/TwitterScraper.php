<?php

namespace App\Console\Commands;

use App\Models\TwitterToken;
use App\Models\TwitterUser;
use Illuminate\Console\Command;
use Thujohn\Twitter\Facades\Twitter;

class TwitterScraper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:scraper {twitter_user?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape twitter and see is param twitter username follow session defined user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $defined_user = env('DEFINED_USER');
        $twitter_user_name = $this->argument('twitter_user');
        if (is_null($twitter_user_name)) {
            $twitter_user = TwitterUser::first();
        }else{
            $twitter_user = TwitterUser::where('username', $twitter_user_name)->first();
        }
        if (!is_null($twitter_user) && !is_null($defined_user)) {
            $twitter_token = TwitterToken::orderBy('id', 'desc')->first();
            if (!is_null($twitter_token)) {
                $token = json_decode(json_encode($twitter_token),TRUE);
                session()->put('access_token', $token);
                $friends = Twitter::getFriends(['screen_name'=>$twitter_user->username]);
                session()->forget('access_token');
                $saved = false;
                foreach($friends->users as $friend){
                    if($friend->screen_name == $defined_user){
                        $twitter_user->update(['follow'=>1]);
                        $saved = true;
                    }
                }if(!$saved){
                    $twitter_user->update(['follow'=>0]);
                }
            }
        }
    }
}
