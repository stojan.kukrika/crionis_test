<?php

namespace App\Console;

use App\Console\Commands\TwitterScraper;
use App\Models\TwitterUser;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Schema;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        TwitterScraper::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if(Schema::hasTable('twitter_users')) {

            // Get all twitter users from the database to check
            $twitterUsers = TwitterUser::select('id', 'username', 'follow')->get();

            // Go through each user to dynamically set them up.
            foreach ($twitterUsers as $user) {
                $schedule->command('twitter:scraper ' . $user->username)->everyMinute();
            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
