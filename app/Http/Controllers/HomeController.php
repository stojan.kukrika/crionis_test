<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScraperUsernameRequest;
use App\Repositories\TwitterUserRepository;

class HomeController extends Controller
{
    /**
     * @var TwitterUserRepository
     */
    private $twitterUserRepository;

    /**
     * HomeController constructor.
     * @param TwitterUserRepository $twitterUserRepository
     */
    public function __construct(TwitterUserRepository $twitterUserRepository)
    {

        $this->twitterUserRepository = $twitterUserRepository;
    }
    /*
     * get list of user who will used for checking friends
     */
    public function index(){
        $twitterUsers = $this->twitterUserRepository->getAll()->get();
        return view('index', compact('twitterUsers'));
    }

    /*
     * put into env variable defined user which will be use for scraper
     */
    public function setScraperAccount(ScraperUsernameRequest $request){

        $path = base_path('.env');
        $env = file($path);
        $env = str_replace('DEFINED_USER=' . env('defined_user'),
                        'DEFINED_USER=' . trim($request->get('defined_user')) . PHP_EOL, $env);

        file_put_contents($path, $env);

        return back();
    }
}
