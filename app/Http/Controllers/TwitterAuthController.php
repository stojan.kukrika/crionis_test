<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Thujohn\Twitter\Facades\Twitter;
use App\Models\TwitterToken;
use Session;

class TwitterAuthController extends Controller
{
    public function twitterAuthenticate()
    {
        $sign_in_twitter = true;
        $force_login = false;

        $token = Twitter::getRequestToken(url('/twitter/callback'));

        if (isset($token['oauth_token_secret'])) {
            $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

            Session::put('oauth_state', 'start');
            Session::put('oauth_request_token', $token['oauth_token']);
            Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

            return redirect($url);
        }

        return redirect(url('/twitter/error'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\UrlGenerator|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function twitterCallback(Request $request)
    {
        if ($request->session()->has('oauth_request_token')) {
            $request_token = [
                'token' => $request->session()->get('oauth_request_token'),
                'secret' => $request->session()->get('oauth_request_token_secret'),
            ];

            Twitter::reconfig($request_token);

            $oauth_verifier = false;

            if ($request->get('oauth_verifier')) {
                $oauth_verifier = $request->get('oauth_verifier');
                $token = Twitter::getAccessToken($oauth_verifier);
            }

            if (!isset($token['oauth_token_secret'])) {
                return url('/twitter/error');
            }

            $credentials = Twitter::getCredentials();

            if (is_object($credentials) && !isset($credentials->error)) {
                $twitterToken = new TwitterToken();
                $twitterToken->oauth_token = $token['oauth_token'];
                $twitterToken->oauth_token_secret = $token['oauth_token_secret'];
                $twitterToken->user_id = $token['user_id'];
                $twitterToken->screen_name = $token['screen_name'];
                $twitterToken->x_auth_expires = $token['x_auth_expires'];

                $twitterToken->save();

                flash(trans('messages.twitter_auth_successful'))->important()->success();
                return redirect('/');
            }

            flash(trans('messages.twitter_auth_failed'))->important()->warning();
            return back();
        }
    }
}
