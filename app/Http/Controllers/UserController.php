<?php

namespace App\Http\Controllers;

use App\Http\Requests\TwitterUserRequest;
use App\Models\TwitterUser;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function create()
    {
        $title = trans('messages.new_user');
        return view('create_user', compact('title'));
    }

    public function store(TwitterUserRequest $request)
    {
        TwitterUser::create($request->all());
        return redirect("/");
    }
}
