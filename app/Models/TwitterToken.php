<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterToken extends Model
{
    protected $guarded = array('id');
}
