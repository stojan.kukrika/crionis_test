<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterUser extends Model
{
    protected $guarded = array('id');

    protected $fillable = ['username', 'follow'];
}
