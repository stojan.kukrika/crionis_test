<?php

namespace App\Providers;

use App\Models\TwitterUser;
use App\Repositories\TwitterUserRepository;
use App\Repositories\TwitterUserRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( TwitterUserRepository::class, function ( $app ) {
            return new TwitterUserRepositoryEloquent( new TwitterUser() );
        } );
    }
}
