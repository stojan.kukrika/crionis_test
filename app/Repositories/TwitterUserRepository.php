<?php

namespace App\Repositories;


interface TwitterUserRepository
{
    public function getAll();
}