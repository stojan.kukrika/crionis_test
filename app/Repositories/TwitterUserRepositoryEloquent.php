<?php

namespace App\Repositories;

use App\Models\TwitterUser;

class TwitterUserRepositoryEloquent implements TwitterUserRepository
{
    /**
     * @var TwitterUser
     */
    private $model;


    /**
     * TwitterUserRepositoryEloquent constructor.
     * @param TwitterUser $model
     */
    public function __construct(TwitterUser $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model;
    }
}