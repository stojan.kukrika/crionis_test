<?php

use Faker\Generator as Faker;

$factory->define(App\Models\TwitterUser::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'follow' => 0
    ];
});

