<?php

use App\Models\TwitterUser;
use Illuminate\Database\Seeder;

class TwitterUsersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TwitterUser::create(['username'=>'7sedam7']);
        TwitterUser::create(['username'=>'crazy_monkey']);
        TwitterUser::create(['username'=>'yellowKiFl4']);
        TwitterUser::create(['username'=>'strumf']);
    }
}
