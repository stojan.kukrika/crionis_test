#Aplikacija za provjeravati pracenje (follow) na twitteru

##Instalacija na xampp ili public hosting:

- Uraditi git clone repositorija
- Iz komandne linije pokrenuti composer install (prethodno instalirati composer sa `https://getcomposer.org/`)
- Kopirati `.env.example` file u fajl `.env`
- U .env fajlu zamjeniti podatke vezane za konekciju ka MySQL serveru
- Iz komandne linije pokrenuti komandu `php artisan kay:generate`
- Iz komandne linije zatim pokrenuti `php artisan migrate` 
- nakon nje `php artisan db:seed` kako bi se pripremila baza za koristenje.
- pokrenuti cron job na serveru `* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1`

Pokrenite aplikaciju posjetom na `http://localhost/crionis/public`

##Instalacija na vagrant(homestead):

- Uraditi git clone repositorija u vagrant projects folder
- Iz komandne linije pokrenuti composer install (prethodno instalirati composer sa `https://getcomposer.org/`)
- Kopirati `.env.example` file u fajl `.env`
- dodati u hosts fajl `192.168.10.10  crionis.test`
- izmjeniti u `Homestead.yaml` fajlu `authorize` i `keys` sa svojim SSH kljucevima
- izmjeniti u `Homestead.yaml` fajlu `folders` sa putajom gdje se projekat nalazi
- uraditi `vagrant up` i `vagrant provision`
- U .env fajlu zamjeniti podatke vezane za konekciju ka MySQL serveru, ovo su predefinisane vrijednosti:
    - DB_DATABASE=crionis_test 
    - DB_USERNAME=homestead
    - DB_PASSWORD=secret
- Iz komandne linije pokrenuti komandu `php artisan kay:generate`
- Iz komandne linije zatim pokrenuti `php artisan migrate` 
- nakon nje `php artisan db:seed` kako bi se pripremila baza za koristenje.
- potrebno je pokrenuti komandu `schedule:run` kako bi se aktivirali cron job-ovi

Pokrenite aplikaciju posjetom na `http://crionis.test`

** Da bi scraper radio potrebno je napraviti aplikaciju na Twitter-u, te zatim izmjeniti API podatke sa onim u `.env`
fajlu (dodani su testni podaci, u `.env.example` file-u)
** Inicijalno, kad se aplikacija opkrene u browser-u potrebno je kliknuti na dugme `Connect to your Twitter account` 
kako bi se aplikacija povezala sa twitter serverom i dobila potreban token za rad