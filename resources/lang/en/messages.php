<?php

return [
    'twitter_auth_token' => 'Connect to your Twitter account',
    'twitter_auth_successful' => 'Successfully connected with your Twitter account',
    'twitter_auth_failed' => 'Connecting to your Twitter account failed',
    'id' => 'Id',
    'follow' => 'Follow user',
    'username' => 'Twitter username',
    'set_scraper_account' => 'Define twitter follow search username',
    'defined_user' => 'Twitter username',
    'save' => 'Save',
    'new_user' => 'Add new user',
    'cancel' => 'Cancel',
    'ok' => 'OK',
];