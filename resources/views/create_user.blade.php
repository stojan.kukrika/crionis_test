@extends('layout.layout')
{{-- Content --}}
@section('content')
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title"> {{$title}}</div>
        </div>
        <div class="panel-body">
            {!! Form::open(array('url' => url('user'), 'method' => 'post', 'files'=> true)) !!}
            <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                {!! Form::label('username', trans('messages.username'), array('class' => 'control-label required')) !!}
                <div class="controls">
                    {!! Form::text('username', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('username', ':message') }}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <a href="{{ url('/') }}" class="btn btn-primary btn-sm">{{trans('messages.cancel')}}</a>
                    <button type="submit" class="btn btn-success btn-sm">{{trans('messages.ok')}}</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop