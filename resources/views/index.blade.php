@extends('layout.layout')
{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-sm-6">
            <a href="{{url('twitter/authenticate')}}" class="btn btn-sm btn-primary">
                {{trans('messages.twitter_auth_token')}}
            </a>
            <br>
            <br>
            @include('flash::message')
            <div>
                <div>{{trans('messages.set_scraper_account')}}</div>
                {!! Form::open(array('url' => url('/save'), 'method' => 'post', 'class' => 'section')) !!}
                <div class="form-group required {{ $errors->has('set_scraper_account') ? 'has-error' : '' }}">
                    {!! Form::label('defined_user', trans('messages.defined_user'), array('class' =>  'control-label')) !!}
                    <div class="controls">
                        {!! Form::text('defined_user', env('DEFINED_USER'), array('class' => 'form-control',
                        'required'=>'required')) !!}
                        <span class="help-block">{{ $errors->first('defined_user', ':message') }}</span>
                    </div>
                </div>
                <div class="form-group text-center">
                    <button type="submit"
                            class="btn btn-sm" id="btn_save">
                        {{trans('messages.save')}}
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-sm-6">
            <a href="{{url('user/')}}" class="btn btn-sm btn-success">{{trans('messages.new_user')}}</a>
            <br>
            <br>
            <table class="table table-hover">
                <thead>
                <tr>
                    <td>{{trans('messages.id')}}</td>
                    <td>{{trans('messages.username')}}</td>
                    <td>{{trans('messages.follow')}}</td>
                </tr>
                </thead>
                <tbody>
                @foreach($twitterUsers as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->follow}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop