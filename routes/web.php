<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::post('/save', 'HomeController@setScraperAccount');
Route::get('twitter/authenticate', 'TwitterAuthController@twitterAuthenticate');
Route::get('twitter/callback', 'TwitterAuthController@twitterCallback');
Route::get('twitter/error', function () {
    dd('Something went wrong');
});

Route::group(['prefix' => 'user'], function () {
    Route::get('/', 'UserController@create');
    Route::post('/', 'UserController@store');
});

